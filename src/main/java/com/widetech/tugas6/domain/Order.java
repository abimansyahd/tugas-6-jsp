package com.widetech.tugas6.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="order_tbl")
public class Order {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
	@Column
	private Date createdAt;
	
	
	public List<OrderItem> getOrderItem(){
		return orderItems;
	}
	
	@OneToMany(mappedBy = "order")
    private List<OrderItem> orderItems;
	
	public double totalPrice() {
		double total = 0;
		for(OrderItem o : orderItems) {
			total += o.totalPrice();
		}
		return (int)total;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
	
}
