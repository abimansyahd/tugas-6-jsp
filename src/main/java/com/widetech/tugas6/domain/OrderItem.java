package com.widetech.tugas6.domain;


import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class OrderItem {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
	@Column
	private Integer quantity;
	
	
	public List<Product> getItems(){
		return products;
	}
	
	public double totalPrice() {
		double total = 0;
		for (Product product : products) {
			total += (product.getPrice()*quantity);
		}
		return (int)total;
	}
	
	@OneToMany(mappedBy = "orderItem")
    private List<Product> products;
	
	 @ManyToOne
	    @JoinColumn(name="order_id")
	    private Order order;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	

	
	
}
