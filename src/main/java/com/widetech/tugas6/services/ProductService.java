package com.widetech.tugas6.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.widetech.tugas6.domain.Order;
import com.widetech.tugas6.domain.Product;
import com.widetech.tugas6.repositories.OrderRepositories;
import com.widetech.tugas6.repositories.ProductRepositories;

@Service
public class ProductService {
	@Autowired
	ProductRepositories repo;
	
	@Transactional
	public List<Product> findAll(){
		return repo.findAll();
	}
	
	@Transactional
	public Product findByCode(String code){
		return repo.findByCode(code);
	}

	@Transactional
	public Product save(Product product) {
		return repo.save(product);
	}
	
	@Transactional
	public Optional<Product> findById(int id){
		return repo.findById(id);
	}
	
	@Transactional
	public void deleteById(int id) {
		repo.deleteById(id);
	}
	
	@Transactional
	public Product updateProduct(int id,Product product) {
		Product updatedProduct = repo.findById(id).orElse(null);
		updatedProduct.setCode(product.getCode());
		updatedProduct.setName(product.getName());
		updatedProduct.setType(product.getType());
		updatedProduct.setPrice(product.getPrice());
		return repo.save(updatedProduct);
	}
	
	
}
