package com.widetech.tugas6.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.widetech.tugas6.domain.OrderItem;
import com.widetech.tugas6.repositories.OrderItemRepositories;

public class OrderItemService {
	@Autowired
	OrderItemRepositories repo;
	
	@Transactional
	public List<OrderItem> findAll(){
		return repo.findAll();
	}
}
