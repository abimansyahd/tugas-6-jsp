package com.widetech.tugas6.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.widetech.tugas6.domain.Order;
import com.widetech.tugas6.repositories.OrderRepositories;

public class OrderService {

	@Autowired
	OrderRepositories repo;
	
	@Transactional
	public List<Order> findAll(){
		return repo.findAll();
	}
}
