package com.widetech.tugas6.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.widetech.tugas6.domain.Product;
import com.widetech.tugas6.services.ProductService;


@Controller
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/list")
	public String showProducts(Model model) {
		List < Product > theProducts = productService.findAll();
		
        model.addAttribute("products", theProducts);
        
        return "product-list";
	}
	
	@GetMapping("/create-product")
    public String showFormForAdd(Model model) {
        model.addAttribute("product", new Product());
        return "product-form";
    }
	
	
	@RequestMapping(value = "/create-product", method = RequestMethod.POST)
	public String createProduct(@ModelAttribute("product") Product product) {
	    productService.save(product);
	    return "redirect:/product/list";
	}

	@RequestMapping(value = "/update-product/{id}")
    public String showUpdateProductPage(@PathVariable int id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("product", productService.findById(id).orElse(null));
        return "update-product";
    }
	
	@RequestMapping(value = "/update-product/{id}", method = RequestMethod.POST)
    public String updateProduct(@PathVariable int id, @ModelAttribute("product") Product product) {
        productService.updateProduct(id, product);
        return "redirect:/product/list";
    }
	
	@RequestMapping(value = "/delete-product/{id}")
    public String deleteProduct(@PathVariable int id) {
        productService.deleteById(id);
        return "redirect:/product/list";
    }

}
