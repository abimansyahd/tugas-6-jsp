package com.widetech.tugas6.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.widetech.tugas6.domain.OrderItem;
import com.widetech.tugas6.domain.Product;

public interface OrderItemRepositories extends CrudRepository<OrderItem, Long> {
	public List<OrderItem> findAll();
	
}
