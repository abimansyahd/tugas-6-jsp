package com.widetech.tugas6.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.widetech.tugas6.domain.Order;

public interface OrderRepositories extends CrudRepository<Order, Long> {
	public List<Order> findAll();
}
