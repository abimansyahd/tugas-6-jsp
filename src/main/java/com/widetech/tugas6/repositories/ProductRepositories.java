package com.widetech.tugas6.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.widetech.tugas6.domain.Product;

public interface ProductRepositories extends JpaRepository<Product, Integer> {
	public List<Product> findAll();
	public Product findByCode(String code);
	
}
