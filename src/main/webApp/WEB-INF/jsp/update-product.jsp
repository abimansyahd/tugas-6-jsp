<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto">
</head>

<style>
body {
	color: #566787;
	background: #f5f5f5;
	font-family: 'Roboto', sans-serif;
}

.table-responsive {
	margin: 30px 0;
}

.table-wrapper {
	min-width: 1000px;
	background: #fff;
	padding: 20px;
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}

.table-title {
	padding-bottom: 10px;
	margin: 0 0 10px;
	min-width: 100%;
}

.table-title h2 {
	margin: 8px 0 0;
	font-size: 22px;
}
</style>


<body>
	<div class="container-xl">
		<div class="table-responsive">
			<div class="table-wrapper">
				<div class="table-title">
					<div class="col">
						<div class="">
							<h2 class="mb-3">
								Update<b>Products</b>
							</h2>
						</div>
						<form:form action="/product/update-product/${id}" method="post"
							modelAttribute="product">
							<div class="mb-2">
								<form:input type="text" class="form-control" id="code"
									placeholder="Code" path="code" />
							</div>
							<div class="mb-2">
								<form:input type="text" class="form-control" id="name"
									placeholder="Name" path="name" />
							</div>
							<div class="mb-2">
								<form:input type="text" class="form-control" id="type"
									placeholder="Type" path="type" />
							</div>
							<div class="mb-2">
								<form:input type="number" class="form-control" id="price"
									placeholder="Price" path="price" />
							</div>

							<div>
								<a href="/product/list"><button type="button"
										class="btn btn-secondary me-3">Cancel</button></a>
								<button type="submit" class="btn btn-primary" value="update">Submit</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>

		</div>
	</div>




</body>
</html>